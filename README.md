

CS 426: Homework 5
LAMP Stack
Background

Wordpress is an extremely popular blogging platform. It is designed to run on a LAMP stack.
Instructions

    Create a mysql puppet module that installs and configures MySQL.
    Create a php module (or add a class to your apache module) that installs php and php-mysql.
    Test your LAMP stack by installing Wordpress.
    You do not need to automate the Wordpress installation.

Deliverables

    Your project with Puppet modules, in Bitbucket as usual.

Requirements

    Wordpress has to work on your server without modifying anything outside of the standard Wordpress install guide

Grade: 100%