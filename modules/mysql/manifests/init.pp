class mysql
{
    package {'mysql':
	ensure => 'installed',
	before =>  Service['mysqld'],
    }	
    package {'mysql-server':
	ensure =>  'installed',
	before =>  Service['mysqld'],
    }
    service {'mysqld' :
	ensure => 'running',
	enable => 'true',
	require => Package['mysql-server'],	
    }
    file {'mysql_secure_installation':
	ensure =>  'file',
	path   =>  '/usr/bin/mysql_secure_installation',
	owner  =>  'root',
	group  =>  'root',
	mode   =>  '0744',
	source =>  'puppet:///modules/mysql/mysql_secure_installation',
    require => Service['mysqld'],	
    }
    file { '/home/root/.my.cnf':
	ensure => 'file',
	path   => '/root/.my.cnf',
	owner  => 'root',
	group  => 'root',
	mode   => '0400',
	source => 'puppet:///modules/mysql/my.cnf',
    }
    exec {'/usr/bin/mysql_secure_installation':
        path         => '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin',
        subscribe    => File['/usr/bin/mysql_secure_installation'],
        user         => 'root',
        refreshonly  => 'true',
        logoutput    => 'true',
        }
}		 
			
	
